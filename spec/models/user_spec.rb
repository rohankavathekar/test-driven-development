require 'rails_helper'

RSpec.describe User, type: :model do
  context 'validation test' do
    it 'ensures firstname present' do
      user = User.new(last_name: 'musk', email: 'elonmusk@specex.co').save
      expect(user).to eq(false)
    end

    it 'ensures lastname present' do
      user = User.new(first_name: 'elon',email: 'elonmusk@specex.co').save
      expect(user).to eq(false)
    end

    it 'ensures email present' do
      user = User.new(first_name: 'elon', last_name: 'musk').save
      expect(user).to eq(false)
    end

    it 'saves successfully' do
      user = User.new(first_name: 'elon', last_name: 'musk', email: 'elonmusk@specex.co').save
      expect(user).to eq(true)
    end

  end
  context 'scope test' do
    let (:params) {{first_name: 'elon' , last_name: 'musk', email: 'elonmusk@specex.co'}}
    before (:each) do
      User.new(params.merge(active: true)).save
      User.new(params.merge(active: true)).save
      User.new(params.merge(active: true)).save
      User.new(params.merge(active: false)).save
      User.new(params.merge(active: false)).save
    end

    it "should return active users" do
      expect(User.active_users.size.class).to eq(Integer)
    end

    it "should return inactive users" do
      expect(User.inactive_users.size.class).to eq(Integer)
    end

  end
end
