require 'rails_helper'

describe Todo, "#completed?" do
  it "returns true" do
    todo = Todo.new(completed_at: Time.current)
    expect(todo).to be_completed
  end
  it "returns false" do
    todo = Todo.new(completed_at: nil)
    expect(todo).not_to be_completed
  end
end
