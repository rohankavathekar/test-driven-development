require 'rails_helper'

RSpec.describe "Users", type: :request do
  context 'GET #index' do
    it 'return a success response' do
      get users_path
      expect(response).to be_successful
    end
  end

  context 'GET #show' do
    it 'returns a success response' do
      user = User.create!(first_name: 'elon', last_name: 'musk', email: 'elonmusk@specex.co')
      get users_path , params: {id: user.to_param}
      expect(response).to be_successful
    end
  end

end
