require 'rails_helper'

feature "all Features are" do
  scenario "Listed" do
    visit root_path
    expect(page).to have_css "h1", text: "Features"
    expect(page).to have_css ".feature li", text: "todo application"
  end
end