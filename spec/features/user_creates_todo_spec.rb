require 'rails_helper'

feature "User Creates Todo" do
  scenario "successfully" do
    sign_in
    click_on "add todo"
    fill_in "Title", with: "Buy milk"
    click_on "Submit"

    expect(page).to have_css '.todo td', text: 'Buy milk'
  end
end