Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :todos, only: [:index, :new, :create ,:show] do
    resource :completion, only: :create
  end
  root to: "home#index"
  resources :users
  resource :session, only: [:new, :create]
end
